from fastai.vision import *

"""
mnist = untar_data(URLs.MNIST_TINY)
print(mnist)
tfms = get_transforms(do_flip=False)
print(tfms)

data = (ImageItemList.from_folder(mnist)
        .split_by_folder()          
        .label_from_folder()
        .transform(tfms, size=32)
        .databunch()
        .normalize(imagenet_stats))

data.show_batch()

learn = create_cnn(data, models.resnet18, metrics=accuracy)
learn.fit_one_cycle(1,1e-2)
learn.save('mini_train')

learn.show_results()
"""

coke_img_path = 'data'
tfms = get_transforms(do_flip=True,flip_vert=True)

data = (ImageItemList.from_folder(coke_img_path)
        .split_by_folder()
        .label_from_folder()
        .databunch()
        .normalize(imagenet_stats))


learn = create_cnn(data, models.resnet18, metrics=accuracy)
learn.fit_one_cycle(1,1e-2)
learn.save('mini_train')
learn.load('mini_train')

learn.show_results()


