from fastai.vision import *

img_path = [
  'aug_09_16.jpg',
  'aug_10_6.jpg',
  'aug_11_4.jpg',
  'aug_13_0.jpg',
  'aug_13_11.jpg',
  'aug_13_15.jpg',
  'aug_13_19.jpg',
  'aug_13_7.jpg',
  'aug_15_15.jpg',
  'aug_15_6.jpg',
  'aug_17_0.jpg',
  'aug_17_5.jpg',
  'aug_17_8.jpg',
  'aug_18_9.jpg',
  'aug_19_1.jpg',
  'aug_19_6.jpg',
  'aug_21_10.jpg',
  'aug_21_14.jpg',
  'aug_21_19.jpg',
  'aug_21_1.jpg',
  'aug_21_7.jpg',
  'aug_22_15.jpg',
  'aug_22_18.jpg',
  'aug_23_12.jpg'
]

img = open_image('data/test/aug_09_16.jpg')
path_prefix = 'data/test/'

learn = load_learner('data');
learn.precompute=False
for p in img_path:
  img = open_image(path_prefix + p)
  print(p,':',learn.predict(img))
