from matplotlib.image import imread
import imageio
import imgaug as ia
from imgaug import augmenters as iaa

sometimes = lambda aug: iaa.Sometimes(0.5, aug)
seq = iaa.Sequential([
    iaa.PadToFixedSize(1600,1600, position='center', pad_mode=ia.ALL, pad_cval=(0, 255)),
    iaa.Scale({'height':0.25,'width':0.25}),
    iaa.Fliplr(0.5), # horizontally flip 50% of the images
    iaa.Flipud(0.5), # 
    iaa.Rot90((0,3), keep_size=False),
    iaa.Sometimes(0.8, iaa.AdditiveGaussianNoise(0,10.0)),
])
# seq = iaa.Sequential([
#     iaa.Crop(px=(0, 16)), # crop images from each side by 0 to 16px (randomly chosen)
#     iaa.Fliplr(0.5), # horizontally flip 50% of the images
#     iaa.GaussianBlur(sigma=(0, 5.0)), # blur images with a sigma of 0 to 3.0
#     iaa.AdditiveGaussianNoise(0,0.1),
#     iaa.fog(),
#     sometimes(iaa.Affine(
#         scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
#         # translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
#         # rotate=(-45, 45),
#         # shear=(-16, 16),
#         order=[0, 1],
#         cval=(0, 255),
#         mode=ia.ALL
#     )),
# ])

all_img = []
for i in range(9,25):
    all_img.append(imread('img01/IMG-20190120-WA00%02d.jpg' %i))

#for batch_idx in range(1000):
#    images_aug = seq.augment_images(all_img)
images_aug = seq.augment_images(all_img)
print(len(images_aug))
base = 9
for i in range(len(images_aug)):
    img = all_img[i]
    for j in range(20):
        aug_img = seq.augment_images([img])
        imageio.imwrite('img01_aug/aug_%02d_%d.jpg' %(i+base,j), aug_img[0])

"""
for batch_idx in range(1000):
    # 'images' should be either a 4D numpy array of shape (N, height, width, channels)
    # or a list of 3D numpy arrays, each having shape (height, width, channels).
    # Grayscale images must have shape (height, width, 1) each.
    # All images must have numpy's dtype uint8. Values are expected to be in
    # range 0-255.
    images = load_batch(batch_idx)
    images_aug = seq.augment_images(images)
    train_on_images(images_aug)
    """
