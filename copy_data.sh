rm data/train/1/*
rm data/train/2/*
rm data/train/3/*

rm data/valid/1/*
rm data/valid/2/*
rm data/valid/3/*

cp img01_aug/*_09_* data/train/1/
cp img01_aug/*_10_* data/train/1/
cp img01_aug/*_16_* data/train/1/
cp img01_aug/*_17_* data/train/1/
cp img01_aug/*_18_* data/train/1/
cp img01_aug/*_19_* data/train/1/

cp img01_aug/*_11_* data/train/2/
cp img01_aug/*_12_* data/train/2/
cp img01_aug/*_13_* data/train/2/
cp img01_aug/*_14_* data/train/2/
cp img01_aug/*_15_* data/train/2/
cp img01_aug/*_15_* data/train/2/

cp img01_aug/*_20_* data/train/3/
cp img01_aug/*_21_* data/train/3/
cp img01_aug/*_22_* data/train/3/
cp img01_aug/*_23_* data/train/3/
cp img01_aug/*_24_* data/train/3/

shuf -zn12 -e data/train/1/*.jpg | xargs -0 mv -vt data/valid/1/
shuf -zn12 -e data/train/2/*.jpg | xargs -0 mv -vt data/valid/2/
shuf -zn12 -e data/train/3/*.jpg | xargs -0 mv -vt data/valid/3/

shuf -zn4 -e data/train/1/*.jpg | xargs -0 mv -vt data/test/
shuf -zn4 -e data/train/2/*.jpg | xargs -0 mv -vt data/test/
shuf -zn4 -e data/train/3/*.jpg | xargs -0 mv -vt data/test/

